package com.example.eltiempotest.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eltiempotest.ApoloData;
import com.example.eltiempotest.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class EventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ApoloData> eventoList,eventHolderList;
    IActionEvento iActionEvento;

    public EventAdapter(Context context, List<ApoloData> eventoList, IActionEvento iActionEvento) {
        this.context = context;
        this.eventoList = eventoList;
        this.eventHolderList = eventoList;
        this.iActionEvento = iActionEvento;
    }

    public interface IActionEvento{
        void gotoReservation(int eventID);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ApoloData evento = eventoList.get(position);

        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.nombreEv.setText(evento.getTitle());
        viewHolder.ciudadEv.setText(evento.getDescription());
        viewHolder.direccionEv.setText(evento.getImageURL());
        Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(viewHolder.image);
        viewHolder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionEvento.gotoReservation(evento.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventoList.size();
    }

    /***
     * ViewHolders
     ***/
    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView nombreEv,direccionEv, ciudadEv, FechaEv;
        ImageView image;
        CardView card;

        public ViewHolder(View itemView) {
            super(itemView);
            nombreEv = (TextView) itemView.findViewById(R.id.name);
            direccionEv = (TextView) itemView.findViewById(R.id.email);
            ciudadEv = (TextView) itemView.findViewById(R.id.mobile);
            card = (CardView) itemView.findViewById(R.id.materialCard);
            image = itemView.findViewById(R.id.image);
        }
    }

    public void searchResults(String query){
        query = query.toLowerCase();
        List<ApoloData>filterList = new ArrayList<>();
        for (ApoloData ev : eventHolderList) {
            final String title = ev.getTitle().toLowerCase();
            if (title.contains(query)) {
                filterList.add(ev);
            }
        }
        this.eventoList = filterList;
        notifyDataSetChanged();
    }
}